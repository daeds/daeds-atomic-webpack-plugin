const processDaedsAtomic = require('daeds-atomic/files');
const getConfig = require('daeds-atomic/config');
const webpackSources = require('webpack-sources');

function DaedsAtomicWebpackPlugin(){
}

DaedsAtomicWebpackPlugin.prototype.apply = function(compiler) {
    const doWork = function(compilation, callback) {
        processDaedsAtomic(true).then(atomic => {
            var asset, concatSource;
            var css = atomic.getCss() || '';
            var output = getConfig(true).output;
            var existingAsset = compilation.assets[output];

            if (!existingAsset) {
                compilation.assets[output] = new webpackSources.RawSource(css);
            }
            else if (css) {
                asset = new webpackSources.RawSource(css);
                concatSource = new webpackSources.ConcatSource(existingAsset.source());
                concatSource.add(asset.source());
                compilation.assets[output] = concatSource;
            }
            
            callback();
        });
    };

    if (compiler.hooks && compiler.hooks.compilation) { // webpack 4+
        const pluginInfo = {name: 'daedsAtomicWebpackPlugin'};

        compiler.hooks.emit.tapAsync(pluginInfo, function(compilation, callback){
            doWork(compilation, callback);
        });
    }
    else { // webpack < 4
        compiler.plugin('emit', function(compilation, callback){
            doWork(compilation, callback);
        });
    }
};

module.exports = DaedsAtomicWebpackPlugin;