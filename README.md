# daeds-atomic-webpack-plugin

Use [daeds-atomic](https://www.npmjs.com/package/daeds-atomic) in your webpack builds.

## Installation

```shell
npm install daeds-atomic-webpack-plugin --save-dev
```

## Usage
Start by creating a daeds-atomic.json config file in your project's root folder. Below is an example file. Please refer to [daeds-atomic](https://www.npmjs.com/package/daeds-atomic) for a more detailed description of the file's content, and also how to write your atomic css classes.

```json
{
    "folders" : [ "/src/components", "/src/templates" ],
    "fileExtensions" : [ "jsx", "html" ],
    "output" : "/build/css/app.css",
    "mobileFirst" : false,
    "variables" : { 
        "mainColor" : "red"
    },
    "overrides" : {
        "myPrefix" : "margin"
    },
    "verbose" : false,
    "alwaysInclude" : ["bgc_@mainColor", "po_static"]
}
```

If your webpack build already creates a css bundle, for example using extract-text-webpack-plugin, then use that path as the output. The css from daeds-atomic will be added at the end of the file.

After the config file is created, add the plugin in your webpack config file.

```javascript
const DaedsAtomicPlugin = require('daeds-atomic-webpack-plugin');

module.exports = {
    
    // ...

    plugins: [
        new DaedsAtomicPlugin()
    ]
};
```