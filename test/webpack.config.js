const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const DaedsAtomicPlugin = require('../daedsAtomicWebpackPlugin');

module.exports = {
    entry: ['./test/src/a.js', './test/src/c.css'],

    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'result.js',
        publicPath: '/build/'
    },

    devtool: 'source-map',

    module: {
        rules: [
            { test: /\.css$/, include: /src/, use: ExtractTextPlugin.extract(['css-loader']) }
        ]
    },

    plugins: [
        new ExtractTextPlugin({ filename: 'css/site.css', allChunks: true }),
        new DaedsAtomicPlugin(),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /site\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: {removeAll: false } },
            canPrint: true
          })
        
          
    ]
};